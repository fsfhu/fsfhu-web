---
Title: "Szabad Szoftver Pályázat 2023 – Eredményhirdetés"
Date: 2023-09-16T10:00:00+02:00
Draft: false
Type: "post"
Summary: ""
Slug: "SzabadSzoftverPalyazat2023-Eredmenyhirdetes"
Author: "Meskó Balázs"
---

Idén összesen 12 pályázatot kaptunk, amelyből 5 esetén döntöttünk úgy, hogy támogatni fogjuk, ezek az alábbiak:

{{< rawhtml >}}
<table class="table table-striped table-compact">
  <thead class="thead-dark">
    <tr>
      <th class="col-3">Pályázók</th>
      <th class="col-6">Cím</th>
      <th class="col-3">Támogatás összege</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="col-3">Baldaszti Zoltán Tamás</td>
      <td class="col-6">{{< /rawhtml >}}
      [Rendszerbetöltő szoftver fejlesztése]({{< staticref "assets/pdf/szszp2023/Baldaszti Zoltán Tamás – Rendszerbetöltő.pdf" >}})
      {{< rawhtml >}}</td>
      <td class="col-3">500 000 Ft</td>
    </tr>
    <tr>
      <td class="col-3">Gajdos Tamás</td>
      <td class="col-6">{{< /rawhtml >}}
      [Rack szekrény légcserélő fejlesztése Arduino és Raspberry Pi alapokon]({{< staticref "assets/pdf/szszp2023/Gajdos Tamás – Légcserélő.pdf" >}})
      {{< rawhtml >}}</td>
      <td class="col-3">90 000 Ft</td>
    </tr>
    <tr>
      <td class="col-3">Dr. Kovács Zoltán</td>
      <td class="col-6">{{< /rawhtml >}}
      [JGEX]({{< staticref "assets/pdf/szszp2023/Kovács Zoltán – JGEX.pdf" >}})
      {{< rawhtml >}}</td>
      <td class="col-3">500 000 Ft</td>
    </tr>
    <tr>
      <td class="col-3">Kuszing János, Szalai Kálmán</td>
      <td class="col-6">{{< /rawhtml >}}
      [A linuxmint.hu verziófrissítése]({{< staticref "assets/pdf/szszp2023/Kuszing János, Szalai Kálmán – linuxmint.hu verziófrissítése.pdf" >}})
      {{< rawhtml >}}</td>
      <td class="col-3">600 000 Ft</td>
    </tr>
    <tr>
      <td class="col-3">Palócz István</td>
      <td class="col-6">{{< /rawhtml >}}
      [A https://drupal.wdh.hu oldal továbbfejlesztése]({{< staticref "assets/pdf/szszp2023/Palócz István – drupal.wdh.hu.pdf" >}})
      {{< rawhtml >}}</td>
      <td class="col-3">500 000 Ft</td>
    </tr>
  </tbody>
</table>
{{</ rawhtml >}}

A többi beküldött, de támogatásban nem részesült pályázat borítója itt található:

- Hollósi Balázs, Szalai Kálmán – [Rendszervédelmi központ fejlesztése Linux felhasználóknak]({{< staticref "assets/pdf/szszp2023/Hollósi Balázs, Szalai Kálmán – Rendszervédelmi központ.pdf" >}})
- Kakuk Sándor – [Linux disztribúció összeállítása az Informatika és távközlés ágazatban szereplő szakmai képzések segítésére]({{< staticref "assets/pdf/szszp2023/Kakuk Sándor – Linux disztribúció összeállítása.pdf" >}})
- Kovács Viktor, Tisza András – [Szabad felhasználású helyesírás-ellenőrző székely-magyar rovásíráshoz]({{< staticref "assets/pdf/szszp2023/Kovács Viktor, Tisza András – Rovás.pdf" >}})
- Nagy Elemér Károly – [Debian 13 telepítő honosítása]({{< staticref "assets/pdf/szszp2023/Nagy Elemér Károly – Debian 13 telepítő honosítása.pdf" >}})
- Nagy Elemér Károly – [TUGIP (TízUjjas GépÍró Program) továbbfejlesztése]({{< staticref "assets/pdf/szszp2023/Nagy Elemér Károly – TUGIP v2.pdf" >}})
- Nagy Elemér Károly – [Xfce 4.18-4.20 magyarítása]({{< staticref "assets/pdf/szszp2023/Nagy Elemér Károly – Xfce.pdf" >}})
- Papp Zoltán – [Adminisztrációs rendszer fejlesztése szociális intézmény részére]({{< staticref "assets/pdf/szszp2023/Papp Zoltán – Adminisztrációs rendszer.pdf" >}})

Köszönjük mindenkinek, aki küldött be pályázatot! A mai napokban felvesszük a kapcsolatot a pályázókkal, és minél hamarabb megkötjük a támogatási szerződéseket. A pályázatban vállalt feladatok 2023. december 30-ig fognak elkészülni, így az eredményekről jövő januárban fogunk beszámolni.
