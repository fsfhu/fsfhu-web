---
Title: "Szabad Szoftver Pályázat 2023"
Date: 2023-08-24T18:40:00+02:00
Draft: false
Type: "post"
Summary: ""
Slug: "SzabadSzoftverPalyazat2023"
Author: "Meskó Balázs"
---

Az FSF.hu Alapítvány pályázatot ír ki szabad szoftverek fejlesztése, népszerűsítése és honosítása témakörben. A pályázók köre: bármely magyarországi természetes vagy jogi személy. A pályázatra biztosított keretösszeg 4 millió forint. Egy pályázó több pályázatot is nyújthat be. Egy pályázat maximális támogatási összege 1 millió forint. A támogatott projektek várható száma: 4-12 pályázat.

A pályázatok kivonatát tartalmazó fedlapokat a tavalyi évhez hasonlóan közzétesszük itt a blogunkon. Illetve a pályázat lezárultával saját összefoglalóval is készülünk az egyes pályázatok eredményéről.

A pályázatok leadási határideje szeptember 9-e, míg a projektek elkészülésének határideje december 30-a. A részletes pályázati kiírás [innen tölthető le]({{< staticref "assets/pdf/szszp2023.pdf">}}).

Minta a pályázat fedlapjához:

* [PDF formátumban]({{< staticref "assets/pdf/szszp2023_fedlap_minta.pdf">}})
* [ODT formátumban]({{< staticref "assets/odt/szszp2023_fedlap_minta.odt">}})

**Jótanácsok a pályázatokhoz:**
 - Hangsúlyozzuk, hogy egy pályázó több pályázatot is leadhat, így ha esetleg egy nagyobb pályázatról van szó, akkor javasoljuk a kisebb részekre szedését, ha ez lehetséges. Ezt azért javasoljuk, mert lehetséges, hogy egy pályázatot egy az egyben nem támogatnánk, de egyes részeit igen.
 - A honosítási projektek esetén szeretnénk tudni, hogy körülbelül mekkora munkáról van szó. A szövegszámok és a szószámok nagyon hasznos információk a számunkra, így kérnénk a források hivatkozását, hogy ezt ellenőrizni tudjuk.
 - Ha valami nem világos, vagy kérdés merült fel, akkor keressetek minket bátran a bejelentéseink alatt, vagy írjatok levelet a [kuratórium e-mail-címére]({{< staticref "kapcsolat" >}}).
