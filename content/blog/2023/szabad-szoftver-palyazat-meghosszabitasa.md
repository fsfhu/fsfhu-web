---
Title: "Szabad Szoftver Pályázat 2023 meghosszabbítása"
Date: 2023-09-09T18:40:00+02:00
Draft: false
Type: "post"
Summary: ""
Slug: "SzabadSzoftverPalyazat2023meghosszabitasa"
Author: "Meskó Balázs"
---
A megkeresésekre való tekintettel négy nappal meghosszabbítjuk a pályázatok leadási határidejét, amely így szeptember 13. éjfele lesz. A kuratóriummal csütörtökön fogjuk elbírálni a pályázatokat, pénteken pedig nyilvánosságra hozzuk az eredményeket.

