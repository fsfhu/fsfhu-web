---
Title: "KDE fordítások 2021-ben az FSF.hu Alapítvány támogatásával"
Date: 2021-12-29T18:11:00+01:00
Draft: false
Type: "post"
Summary: ""
Slug: "KDEHonosítás"
Author: "Kiszel Kristóf"
---

A KDE magyar fordítása jól, de nem tökéletesen áll. Az alapul szolgáló KDE Frameworks 5, a felhasználók által használt Plasma asztali környezet és a legutóbbi marketing innovációnak hála KDE Gear néven futó alkalmazásgyűjtemény több-kevesebb hiányosságot tartalmazott. Ezért az FSF.hu Alapítvánnyal közösen meghatároztuk ezekből azt a minimumot, amit célként kitűztünk erre évre, alapul pedig a Kubuntut vettük, hogy egy telepítés a következő kiadás teljesen magyarul szóljon a felhasználóhoz. Az év során ezért a hiányzó fordítások pótlásán, és a már készek karbantartásán dolgoztam az Alapítvány támogatásával, amit ezúton is köszönök nekik.

Milyen alkalmazásokról van szó? A spektrum elég széles, a Dolphin fájlkezelőtől kezdve az Elisa zenelejátszón, játékokon és rendszeradminisztrációs eszközökön (partíciókezelő, naplómegjelenítő) a KTorrentig. A munka során egy virtuális gépbe telepített Neon disztribúciót használtam, hogy a fordítási fájlok konvertálása és másolgatása nélkül is tesztelni tudjam az eredményt, és igazítani, vagy akár nyomkodni az alkalmazásokat, hogy jobban kiismerjem az adott szöveg kontextusát (nagyon hasznos segítséget tudnak írni egyes fejlesztők…).

Mikor lesz látható az eredménye ennek? Ha valaki rolling release disztribúciót használ, annak hamarabb, a többieknek a saját disztribúciójuk következő kiadásakor, erről kérdezze meg mindenki csomagolóját és karbantartóját.

2022-ben a terv az eddigi eredmények megtartása, és újabb alkalmazások, illetve a dokumentáció fordítása. Szívesen fogadom a közösség javaslatait, mik azok a sokak által használt alkalmazások, amik esetleg rosszul jelennek meg magyarul.
