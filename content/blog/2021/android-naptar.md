---
Title: "Naptárkezelés szabad szoftverekkel"
Date: 2021-01-16T20:46:00+01:00
Draft: false
Type: "post"
Summary: ""
Slug: "Naptárkezelés"
Author: "Meskó Balázs"
---

<!--### Kiindulópont-->

Mostanában elhatároztam, hogy jobban rendet teszek a naptáraim és a névjegyeim közt. A kiindulópont az adatok terén a következő volt:

* alapítványi naptárak és névjegyek a belső Nextcloud példányunkon,
* egy saját naptár és névjegyzék a mailbox.org-nál lévő postafiókomban,
* kettő [GetTogether](https://gettogether.community/) eseménynaptár.

Ezeket szeretném külön, de mégis egy helyen tudni. A Nextcloud és a Mailbox CardDAV-ot és CalDAV-ot használ, a GetTogether pedig ICS/iCalendar naptárakat biztosít.

### Thunderbird

A számítógépeimen *Thunderbirdöt* használok, így természetes volt az igény, hogy abban szeretném tárolni az információkat. Az iCalendar naptárakat szépen kezeli, ezeket minden további nélkül fel tudtam venni. A CalDAV naptárakat is be lehet állítani, de ahogy láttam alapból nem támogatja a CardDAV-ot, így erre mindenképp más megoldást kell kereseni.

A megoldás után keresve rátaláltam a [Nexcloud dokumentációjára](https://docs.nextcloud.com/server/19/user_manual/pim/sync_thunderbird.html), amely a [TbSync kiegészítőt](https://addons.thunderbird.net/hu/thunderbird/addon/tbsync/) javasolta, illetve az ehhez tartozó [CalDAV/CardDAV-szolgáltatót](https://addons.thunderbird.net/hu/thunderbird/addon/dav-4-tbsync/).

Feltelepítettem ezeket, és igen gyorsan be tudtam állítani a szinkronizálást, az automatikus felfedezésnek köszönhetően pillanatokon belül kész is voltam, és látszottak a naptárak és a névjegyek. Az időszakos szinkronizálást is beállítja a kiegészítő, így kész is vagyunk.

### Android

Az okostelefonommal eddig egyiket sem szinkronizáltam, így itt számomra lényegében zöldmezős projekt volt. Alapvetően szerettem volna az [F-Droidban](https://f-droid.org/) elérhető szoftverekkel megoldani a dolgot, és ezt szerencsére kompromisszumok nélkül sikerült megtenni.

A naptár és névjegykezelés része az Android alapvető része (egy okostelefonokra szánt operációs rendszeren ez aligha meglepő), így némileg más a szemlélet szoftveres oldalon is. Az [Etar naptárprogram](https://f-droid.org/en/packages/ws.xsoh.etar/), amire végül a választásom esett, alapvetően a rendszerbe regisztrált naptárokat használja, maga nem képes távoli naptárakat kezelni (és ez nem is célja). Ez szerencsére valójában  nem probléma, mert jól kiegészíthető két másik alkalmazással, ezek az [ICSx⁵](https://f-droid.org/en/packages/at.bitfire.icsdroid/) és a [DAVx⁵](https://f-droid.org/en/packages/at.bitfire.davdroid/). Mindhárom alkalmazás GPLv3 licenc alatt közzétett szabad szoftver.

A nevükből ki is található hogy mire valók, az ICSx⁵ szinkronizálja az iCalendar naptárokat, a DAVx⁵ pedig a CalDAV naptárakat és a CardDAV névjegyeket.

#### Naptárak hozzáadása az ICSx⁵-ben

A GetTogether minden egyes csapathoz iCalendar naptárat biztosít, egyszerűen ki kell nyernünk a hivatkozást a csapat kezdőoldaláról. A miénk így néz ki:

![FSF.hu GetTogether csapat]({{< staticref "assets/images/blog/2021/gettogether-csapat.png">}} "FSF.hu GetTogether csapat")

Asztali böngészőben a helyi menüből, mobilböngészőben hosszú koppintásra érhető el a hivatkozás másolása. Egyszerűen be kell másolni az ICSx⁵ URL mezőjébe.

![ICSx⁵ – Hozzáadás]({{< staticref "assets/images/blog/2021/icsx5-hozzaadas.jpg">}} "ICSx⁵ – Hozzáadás")
![ICSx⁵ – Lista]({{< staticref "assets/images/blog/2021/icsx5-lista.jpg">}} "ICSx⁵ – Lista")

Felvettem a saját csoportjaim naptárát, majd beállítottam hozzá egy megkülönböztető színt. Ezzel be is kerültek a rendszerbe a naptárak, ahogy azt majd a későbbiekben látni fogjuk.

#### Naptárak hozzáadása a DAVx⁵-ben

A CalDAV/CardDav fiókok hozzáadása egy fokkal macerásabb, mert hitelesítést igényelnek. A Nextcloud fiókat simán hozzá tudtam adni az e-mail-címem és a jelszavam megadásával, a mailboxos.org-fiókomnál viszont ez nem működött, itt meg kellett néznem a szolgáltatómnál, hogy milyen címet kell megadni.

![DAVx⁵ – Hozzáadás]({{< staticref "assets/images/blog/2021/davx5-hozzaadas.jpg" >}} "DAVx⁵ – Hozzáadás")
![DAVx⁵ – Beállítások]({{< staticref "assets/images/blog/2021/davx5-beallitasok.jpg" >}} "DAVx⁵ – Beállítások")
![DAVx⁵ – Lista]({{< staticref "assets/images/blog/2021/davx5-lista.jpg" >}} "DAVx⁵ – Lista")

Mindenesetre ez is gyorsan sikerült. A CalDAV esetén egyesével szabályozhatjuk, hogy mely naptárakat akarjuk szinkronizálni, és melyeket nem, illetve csak olvashatóvá tehetjük a naptárakat, ha szerkeszteni nem szeretnénk. Alapbeállításban még a színeket is átveszi a Nextcloudtól, így azzal sem volt külön teendőm.

#### A végeredmény, naptárak az Etarban

Mindezekkel végezve négy naptár lett hozzáadva a rendszerhez, melyet láthatunk is az Etar *Beállítások* oldalán. A CalDAV-naptárak a megfelelő fióknevekkel szerepelnek, míg az iCalendar-naptárak egy általános *Naptárfeliratkozások* listában jelennek meg.

![Etar – Beállítások]({{< staticref "assets/images/blog/2021/etar-beallitasok.jpg" >}} "Etar – Beállítások")
![Etar – Naptár]({{< staticref "assets/images/blog/2021/etar-naptar.jpg" >}} "Etar – Naptár")

Maga a naptáralkalmazás egyébként nagyon egyszerű, de tudja mindazt, amire szükségem van. A projekt egyébként a gyári AOSP naptárra épül.

### Letöltés

A könnyű elérhetőség miatt összeszedtem az alkalmazások legfontosabb hivatkozásait. Mindhárom elérhető Google Playből és F-Droidból is, viszont az ICSx⁵ és a DAVx⁵ a Play Áruházban fizetős, a cikk írásának pillanatában az előbbi 699&nbsp;Ft, utóbbi pedig 1&nbsp;790&nbsp;Ft.

| | | |
|-|-|-|
| **Etar** | [{{< imageref src="assets/images/get-it-on-fdroid.png" label="Etar az F-Droidon" class="btn-fdroid" >}}](https://f-droid.org/hu/packages/ws.xsoh.etar/) | [{{< imageref src="assets/images/get-it-on-play.png" label="Etar a Google Playen" class="btn-googleplay" >}}](https://play.google.com/store/apps/details?id=ws.xsoh.etar&hl=hu) |
| **ICSx⁵** | [{{< imageref src="assets/images/get-it-on-fdroid.png" label="ICSx⁵ az F-Droidon" class="btn-googleplay" >}}](https://f-droid.org/hu/packages/at.bitfire.icsdroid/) | [{{< imageref src="assets/images/get-it-on-play.png" label="ICSx⁵ a Google Playen" class="btn-fdroid" >}}](https://play.google.com/store/apps/details?id=at.bitfire.icsdroid&hl=hu) |
| **DAVx⁵** | [{{< imageref src="assets/images/get-it-on-fdroid.png" label="DAVx⁵ az F-Droidon" class="btn-googleplay" >}}](https://f-droid.org/hu/packages/at.bitfire.davdroid/) | [{{< imageref src="assets/images/get-it-on-play.png" label="DAVx⁵ a Google Playen" class="btn-fdroid" >}}](https://play.google.com/store/apps/details?id=at.bitfire.davdroid&hl=hu) |


**Jogi információk:** A „Get it on F-Droid” jelvény [CC BY-SA 3.0 licenc](https://creativecommons.org/licenses/by-sa/3.0/) alatt érhető el. A Google Play és a Google Play-logó a Google LLC védjegyei.

