---
Title: "Szabad Szoftver Pályázat 2022 – Eredményhirdetés"
Date: 2022-11-07T10:00:00+02:00
Draft: false
Type: "post"
Summary: ""
Slug: "SzabadSzoftverPalyazat2022-Eredmenyhirdetes"
Author: "Meskó Balázs"
---

Elérkezett az idei Szabad Szoftver Pályázatunk leadási ideje. Összesen 18 pályázatot kaptunk, amelyből 5 esetén döntöttünk úgy, hogy támogatni fogjuk, ezek az alábbiak:

{{< rawhtml >}}
<table class="table table-striped table-compact">
  <thead class="thead-dark">
    <tr>
      <th class="col-3">Pályázók</th>
      <th class="col-6">Cím</th>
      <th class="col-3">Támogatás összege</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="col-3">Hollósi Balázs, Szalai Kálmán, Vásony Tamás</td>
      <td class="col-6">{{< /rawhtml >}}
      [OSM POI Matchmaker]({{< staticref "assets/pdf/szszp2022/Szalai Kálmán – POI matchmaker.pdf" >}})
      {{< rawhtml >}}</td>
      <td class="col-3">800 000 Ft</td>
    </tr>
    <tr>
      <td class="col-3">Kiss Kinga Zita</td>
      <td class="col-6">{{< /rawhtml >}}
      [Rocket.Chat honosítása]({{< staticref "assets/pdf/szszp2022/Kiss Kinga Zita – RocketChat.pdf" >}})
      {{< rawhtml >}}</td>
      <td class="col-3">350 000 Ft</td>
    </tr>
    <tr>
      <td class="col-3">Kiszel Kristóf</td>
      <td class="col-6">{{< /rawhtml >}}
      [Krusader honosítása]({{< staticref "assets/pdf/szszp2022/Kiszel Kristóf – Krusader.pdf" >}})
      {{< rawhtml >}}</td>
      <td class="col-3">75 210 Ft</td>
    </tr>
    <tr>
      <td class="col-3">Dr. Nagy Elemér Károly</td>
      <td class="col-6">{{< /rawhtml >}}
      [Gépírást oktató program]({{< staticref "assets/pdf/szszp2022/Dr. Nagy Elemér Károly – Gépírást oktató program.pdf" >}})
      {{< rawhtml >}}</td>
      <td class="col-3">825 000 Ft</td>
    </tr>
    <tr>
      <td class="col-3">Palócz István</td>
      <td class="col-6">{{< /rawhtml >}}
      [CMS oktatási segédanyagok tanároknak]({{< staticref "assets/pdf/szszp2022/Palócz István – CMS oktatóanyag.pdf" >}})
      {{< rawhtml >}}</td>
      <td class="col-3">900 000 Ft</td>
    </tr>
  </tbody>
</table>
{{</ rawhtml >}}

A többi beküldött, de támogatásban nem részesült pályázat borítója itt található:

- Baldaszti Zoltán Tamás – [TirNanoG]({{< staticref "assets/pdf/szszp2022/Baldaszti Zoltán Tamás – TirNanoG.pdf" >}})
- D. Nagy Gergő – [Root: Laravel alapú admin csomag (tovább)fejlesztése]({{< staticref "assets/pdf/szszp2022/D. Nagy Gergő - Root.pdf" >}})
- Darvas Roland, Szervác Attila, Dr. Nagy Elemér Károly – [Debian 12 telepítő, Xfce 4.18 CD-ISO és Glaxnimate fordítása]({{< staticref "assets/pdf/szszp2022/Darvas Roland – Debian telepítő.pdf" >}})
- Hollósi Balázs, Szalai Kálmán – [Rendszervédelmi központ fejlesztése Linux felhasználóknak]({{< staticref "assets/pdf/szszp2022/Szalai Kálmán – Rendszervédelmi központ.pdf" >}})
- Dr. Kovács Zoltán – [JGEx honosítása]({{< staticref "assets/pdf/szszp2022/Dr. Kovács Zoltán – JGEx.pdf" >}})
- Dr. Kovács Zoltán – [OK Geometry honosítása]({{< staticref "assets/pdf/szszp2022/Dr. Kovács Zoltán – OK Geometry.pdf" >}})
- Orbán Levente – [dyrectorio: felhő alapú szoftverek telepítését elősegítő PaaS alkalmazás]({{< staticref "assets/pdf/szszp2022/Orbán Levente – dyrectorio.pdf" >}})
- Palócz István – [A drupal.wdh.hu oldal továbbfejlesztése]({{< staticref "assets/pdf/szszp2022/Palócz István – drupal-wdh-hu.pdf" >}})
- Palócz István – [KMOOC Drupal tananyag frissítés]({{< staticref "assets/pdf/szszp2022/Palócz István – KMOOC.pdf" >}})
- Papp Zoltán – [Adminisztrációs rendszer fejlesztése szociális intézmény részére]({{< staticref "assets/pdf/szszp2022/Papp Zoltán – Adminisztrációs rendszer.pdf" >}})
- Szalai Kálmán – [A linuxmint.hu oldal népszerűsítési törekvéseinek támogatása a weboldal automatizálásának fejlesztésével]({{< staticref "assets/pdf/szszp2022/Szalai Kálmán – dlvrit.pdf" >}})
- Szalma János – [Mesterséges intelligencia alapú szoftver fejlesztése genomikai adatok automatizált kiértékelésére]({{< staticref "assets/pdf/szszp2022/Szalma János – MI genomika.pdf" >}})
- Tóth Milán – [Felhasználóbarát csempéző ablakozó asztali réteg és hozzátartozó alkalmazások fejlesztése]({{< staticref "assets/pdf/szszp2022/Tóth Milán – SwayOS.pdf" >}})

Köszönjük mindenkinek, aki küldött be pályázatot! A következő napokban felvesszük a kapcsolatot a pályázókkal, és megkötjük a támogatási szerződéseket.
A pályázatban vállalt feladatok 2022. december 25.-ig fognak elkészülni, így az eredményekről jövő januárban fogunk beszámolni.
