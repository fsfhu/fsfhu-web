---
Title: "22. Open Source Budapest meetup"
Date: 2018-02-14T22:34:26+02:00
Draft: false
Type: "post"
Summary: "22. Open Source Budapest meetup"
Slug: "OSBP22"
Author: "Úr Balázs"
---

Az FSF.hu Alapítvány és a >magyar Mozilla közösség ismét meetupot szervez a szabad szoftverekről. Célunk a nyílt forrású szoftverek használata mellett az azok fejlesztésében való részvétel népszerűsítése. Ezen kívül ez a meetup találkozási lehetőséget biztosít a nyílt forráskódú szoftvereket használó fejlesztőknek és felhasználóknak.

Az eddigi meetupokkal ellentétben most csak egyetlen témáról lesz szó a megszokottnál részletesebb, hosszabb előadással és beszélgetési lehetőséggel. Boskovits Gábor interaktív előadásában betekintést nyerhetsz a **GuixSD** világába. A GuixSD egy olyan GNU disztribúció, amely teljes mértékben szabad, megbízható és barkácsolható. Bővebb információk a [GuixSD weboldalán][1] olvashatók, és természetesen Boskovits Gábor előadásában.

Az előadás után GPG-kulcs aláírást is szervezünk, így ha van GPG-kulcsod, akkor feltétlenül hozd magaddal!

**Időpont:** 2018. február 20., kedd 17:30-21:00

**Helyszín:** [D18 Irodaház és Kávézó][2] (1066 Budapest, Dessewffy utca 18-20.)

A legutóbbi meetup előadásairól készült felvétel itt nézhető meg: [Open Source Budapest YouTube-csatorna][3]

Jelentkezz előadónak!
- Fejlesztesz valamit, vagy akár csak van egy ötleted, amibe másnak is érdemes lenne beszállni?
- A cégetek szabad szoftvert használ a LAMP szervernél izgalmasabb feladatra?
- A szabadon elérhető tudás megváltoztatta az életed?
- Találtál egy menőbb Emacs billentyűkombinációt a C-x M-c M-butterfly-nál?

Meséld el nekünk! Írd meg előadásötleted – és hogy melyik páros hónap második keddjén érsz rá. Jelentkezz a szervezőnél: https://www.urbalazs.hu/

[1]: https://www.gnu.org/software/guix/
[2]: https://www.openstreetmap.org/node/5343540922
[3]: https://www.youtube.com/user/opensourcebudapest
