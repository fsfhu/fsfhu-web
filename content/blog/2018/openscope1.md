---
Title: "I. OpenScope este"
Date: 2018-01-16T20:30:26+02:00
Draft: false
Type: "post"
Summary: "I. OpenScope este"
Slug: "OpenScope1"
Author: "Meskó Balázs"
---

Az FSF.hu Alapítvány kiemelt célja a szabad szoftverek honosítása. Ezért most rendezvénysorozatot indítunk az OpenScope projekt keretében, célunk az új emberek bevonása a fordítási erőfeszítéseinkbe. A rendezvények célja rövid fordítói sprintek tartása, melyek során egy-egy adott feladatra, feladatcsoportra koncentrálunk. Az egyes események mindig egy adott projekthez fognak kapcsolódni, és főleg azok megismeréséről, illetve fordítási módszereik elsajátításáról fognak szól. A konkrét projekt mellett pedig kifejezetten várjuk a saját ötleteket, projekteket. Az eseményre egyaránt várjuk a tapasztalt fordítókat, és a teljesen kezdőket is. Mindenki megtalálja a neki való feladatot, csatlakozzatok bátran!

A legelső eseményt 2018. január 18.-án este lesz, a magyar Mozilla közösség jóvoltából a [D18 Irodaházban][1]. Az első témánk a [KDE][2] lesz, melynek aprója egy [friss HUP szavazás][3], mely szerint elég népszerű :) A jövőben kéthetente fogunk hasonló rendezvényeket tartani, tehát minden páratlan csütörtökön.

Összegezve a dolgokat:
- I. OpenScope este
- 2018. január 18., 18:00-21:00
- [D18 Irodaház][4], 1066 Budapest, Dessewffy utca 18-20.

Ott találkozunk!

Meskó Balázs, eseményszervező

[1]: http://d18.hu
[2]: https://www.kde.org/
[3]: https://hup.hu/szavazasok/20171215/hovd_2017_kedvenc_desktop_kornyezet
[4]: https://www.openstreetmap.org/node/5343540922

