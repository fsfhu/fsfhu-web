---
Title: "Magyar nyelvi fejlesztések II–III. (kivonat)"
Date: 2018-01-17T23:07:04+02:00
Draft: false
Type: "post"
Summary: "Magyar nyelvi fejlesztések II–III. (kivonat)"
Slug: "NyelviFejlesztések"
Author: "Gyaraki László"
---

A LibreOffice-hoz kapcsolódó, az FSF.hu Alapítvány támogatásával megvalósított magyar nyelvi fejlesztések legfrissebb eredményeinek összefoglalója:

**Valódi bővíthető helyesírási szótár.** A korábbi <!-- FIXME: belső hivatkozás --> „Nyelvi minta” példa mellett „[Grammar By][1]” (angol) és „[Grammatik nach][2]” (német) videók is készültek a LibreOffice 6.0 egyik leghasznosabb újdonságáról. Az egyéni szótárba felvett új szavainkat, ha megadunk hozzájuk egy-egy mintaszót is, a LibreOffice helyesírás-ellenőrzője, a Hunspell tökéletesen toldalékolja, és szóösszetételekben is felismeri, egy csapásra eltüntetve a bosszantó piros aláhúzásokat az új szavak minden előfordulásáról a szövegben. 

**Régi, de jó**. A régi magyar helyesírás szeptemberig még érvényes a közoktatásban. A LibreOffice-ból sem tűnik el véglegesen: a **Régi helyesírás (AkH. 11.)** nevű új felhasználói szótárral bekapcsolható, így a „csodaszép Vietnam” esetében nem jelez hibát a program, még ha ez már „csoda szép Vietnám”-nak írandó is az iskolán kívül. A toldalékoló felhasználói szótár használatát [bemutató videó][3].

**Segít a kiejtés!** Ha nem ismerjük pontosan egy idegen írásmódú szó helyesírását, írhatjuk most már fonetikusan is, mivel a helyesírás-ellenőrző most már minden szótári szó esetben képes helyes javaslatot tenni. *Korábban gyakoriak voltak a rossz javaslatok:*
- donkihote → tejkihordó
- elnínyó → elsínyled
- elnínyót → elsínylett
- kroaszonokat → croissant-osokat, aszinkronokat, Kroiszoszokat, stb.
- porsésokat → porsasokét, porsásokat, porsósokat, porsasokat, stb.
- russzó → tusszó, árusszó, résszó, rumszó
- russzóig → tusszóig, árusszóig, résszóig, rumszóig, Rusóiig, Russig
- volter → voltér, voltere, volterő, voltper, volterű voltertől → voltértől, volterétől, volterőtől, voltpertől, stb.

A LibreOffice megújult Hunspell helyesírás-ellenőrzője pontos javaslatokat ad:
- donkihote → Don Quijote
- elnínyó → El Niño
- elnínyót → El Niñót
- kroaszonokat → croissant-okat
- porsésokat → porschésokat, Porschésokat
- russzó → Rousseau
- russzóig → Rousseau-ig
- volter → Voltaire
- voltertől → Voltaire-től

További újdonságok és példák a [LibreOffice.hu][4] weboldalon olvashatók.

[1]: https://youtu.be/EsS3gaBTfOo
[2]: https://youtu.be/aYVFDqCUb6I
[3]: https://youtu.be/vgGMSRrPA64
[4]: http://libreoffice.hu/magyar-nyelvi-fejlesztesek-ii/
