---
Title: "Szabad Szoftver Konferencia 2018"
Date: 2018-04-26T21:00:00+02:00
Draft: false
Type: "post"
Summary: "Elindult a regisztráció a 2018. május 12-ei Szabad Szoftver Konferenciára..."
Slug: "SzSzK2018"
Author: "Meskó Balázs"
---
Elindult a regisztráció az idei Szabad Szoftver Konferenciára, amelyre
_2018. május 12_-én kerül sor az _ELTE Lágymányosi Campusának Északi
épületében_. 
A konferencián négy szekcióban párhuzamosan zajlanak majd az előadások. 
Az előadások leírása megtalálható a konferencia honlapján. Több szabad
szoftveres közösség és támogatóink (Andrews, Novell, Rackforest, Serverside, 
NISZ) is készülnek kiállítói asztallal, így az előadások közötti szünetekben
sem maradtok program nélkül. Idén is készülünk értékes nyereményekkel, sorsolunk
pólókat, és díjazzuk a konferencia legjobb előadóját.

A konferencián a részvétel ingyenes, de regisztrációhoz kötött. 
További információk és regisztráció a konferencia honlapján (https://konf.fsf.hu).

