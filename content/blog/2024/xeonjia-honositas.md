---
Title: "Szappanjaitok fagyasztott körtékké válnak"
Date: 2024-12-16T11:30:00+02:00
Draft: false
Type: "post"
Summary: "A Xeonjia játék történetének gépi fordítása során elkövethető hibák a szövegkörnyezet figyelmen kívül hagyása miatt."
Slug: "Xeonjia-honositas"
Author: "Úr Balázs"
---

A címben szereplő mondat gépi fordítás eredménye. Úgy tűnik, hogy a gép hibázott, és értelmetlen magyar mondatot hozott létre. Jobban megvizsgálva azonban látni fogjuk, hogy a gép hibázott ugyan, de nem abban, amire először gondolnánk. Tökéletesen lefordította a bemenetként kapott értelmetlen angol mondatot értelmetlen magyar mondattá. Hiányzott ugyanis egy lényeges információ, a szövegkörnyezet. Ha minőségi fordításra van szükség, akkor a gépek nem fogják elvenni az emberi fordítók munkáját. Legfeljebb gyorsabbá teszik azt.

A gépi fordítás nagymértékben leegyszerűsíti a fordítók munkáját. A fordítások minősége megfelelő, sokszor egyáltalán nem is kell módosítani a javaslatokon. Vannak azonban olyan esetek, amikor egy szó több jelentéséből a szövegkörnyezetnek megfelelőt kell kiválasztani, illetve ha nem tükörfordításra van szükség, hanem kreatívan kell megoldani a feladatot. Ezeket a gépi fordítók még nem tudják, és a fordítás eredménye szinte használhatatlan lesz.

A szoftverek fordításai nyelvi fájlokban találhatók. Ezek a fájlok a szoftverben megjelenített karakterláncokat (szavakat, mondatokat stb.) tartalmazzák, de csak egy listában, egymás után felsorolva azokat. Sokszor az egymás után következő karakterláncok sorrendje is eltérő a szoftverben megjelenő helyétől. Ennek az az oka, hogy a nyelvi fájlokban az azonos karakterláncok csak egyszer fordulnak elő, a karakterláncok összegyűjtése pedig a forráskódban lévő fájlok sorrendjétől függ. Ráadásul a fordítók a karakterláncokat a legtöbb esetben szövegkörnyezet nélkül látják.

Gyakori probléma az azonos alakú, de több jelentéssel bíró szavak hibás fordítása. Ha a fordítás úgy készül, hogy a fordító nem nézi azt, hogy a karakterlánc hol és mikor jeleik meg a szoftverben, akkor magáról a szóról lehetetlen eldönteni, hogy melyik jelentését kell magyarra fordítani. Egy kis többletmunkával a fordító ezt tudja ellenőrizni, azonban gépi fordítás használatakor ez a lépés biztosan kimarad. Így lesz a kriptovaluta-tárcának tőkesúlya egyenleg helyett (balance = egyenleg, tőkesúly), így kerülhet tábornok a beállítások általános lapjára (general = általános, tábornok), így lehet egy személynek címe a regisztrációs lapon titulus helyett (title = titulus, cím), vagy így lehet egy objektumnak állama állapot helyett (state = állapot, állam). Sok ilyen szó van még, lehetne még folytatni a felsorolást.

Most térjünk vissza a címben lévő mondatra. Ezzel a mondattal a [Xeonjia](https://gitlab.com/deepdaikon/Xeonjia/) játék fordítása közben találkoztam. A játék története nagyszerűen fordítható géppel. Sok szöveget tartalmaz, egyáltalán nincsenek benne műszaki szakszavak, a párbeszédek nagyon közel állnak az emberek közötti természetes beszédhez. Van azonban a történetben néhány olyan rész, amelyet nem szabad szó szerint lefordítani, hanem figyelembe kell venni a szövegkörnyezetet, ahol a mondatok elhangzottak.

Nézzünk erre egy példát. A hős elérkezett a gonoszhoz a társával együtt, és az alábbi párbeszéd zajlott le köztük:

```
[Főgonosz]
My glacial domination is about to begin, and you will watch the
dawn of this new era while your hopes turn into frozen tears.

[Hős a társához]
I liked his speech.

[Hős, a gonosz hangját utánozva]
Your copes turn into chosen fears.

[Hős társa]
I don't recall him saying that.

[Hős, a gonosz hangját utánozva]
Your soaps turn into frozen pears.

[Hős társa]
Mmh… It wasn't even like that.

[Hős a főgonoszhoz]
Let’s fight.
```

A gépi fordítás az alábbi magyar szöveget adta eredményül:

```
[Főgonosz]
Jéghideg uralmam hamarosan elkezdődik, és ti végignézitek majd ennek az
új korszaknak a hajnalát, miközben reményeitek fagyott könnyekké válnak.

[Hős a társához]
Tetszett a beszéde.

[Hős, a gonosz hangját utánozva]
Küzdelmeitek választott félelmekké válnak.

[Hős társa]
Nem emlékszem, hogy ezt mondta volna.

[Hős, a gonosz hangját utánozva]
Szappanjaitok fagyasztott körtékké válnak.

[Hős társa]
Mmh… Még csak nem is így volt.

[Hős a főgonoszhoz]
Harcoljunk!
```

Nem lehetne okunk a panaszra, mert a gépi fordítás nagyon jól visszaadta magyarul az angol szöveget. Csakhogy itt ez nem megfelelő, pontosan a lényeget nem tudja visszaadni, mert a mondatokat egymástól függetlenül fordítja, nem tudja azokat egységben kezelni. Adatot kapunk információ helyett, a mondatokat egyesével, különálló bemeneti adatként kapja meg, és dolgozza fel.

Jó pár perc gondolkodás után a gépi fordítást felülbírálva, az alábbi magyar mondatok kerültek be a játékba:

```
[Főgonosz]
Jéghideg uralmam hamarosan elkezdődik, és ti végignézitek majd ennek az
új korszaknak a hajnalát, miközben reményeitek fagyott könnyekké válnak.

[Hős a társához]
Tetszett a beszéde.

[Hős, a gonosz hangját utánozva]
Regényeitek faragott könyvekké válnak.

[Hős társa]
Nem emlékszem, hogy ezt mondta volna.

[Hős, a gonosz hangját utánozva]
Eredményeitek hagyott környékké fájnak.

[Hős társa]
Mmh… Még csak nem is így volt.

[Hős a főgonoszhoz]
Harcoljunk!
```

Más így, ugye? Aki magyarul játszik a játékkal, így ő is megérti a hős gúnyolódását az olvasott szöveg alapján. Ez a kreativitás hiányzik a gépi fordítókból. Kérdés, hogy ez változik-e majd a jövőben, vagy megmaradnak ezek a rendszerek egyszerű fordítónak. Mindenesetre ha jó minőségű fordításra van igény, akkor az emberi fordítónak át kell olvasni a gépi fordítás eredményét. Az emberek visszaveszik a gépek munkáját.

A Xeonjia játék fordítását [Úr Balázs](https://www.urbalazs.hu/), az FSF.hu Alapítvány önkéntese készítette. Élvezetes játékot kívánunk mindenkinek!
