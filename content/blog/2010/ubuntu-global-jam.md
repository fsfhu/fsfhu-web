---
Title: "Ubuntu Global Jam"
Date: 2010-03-10T20:00:00+02:00
Draft: false
Type: "post"
Summary: "Ubuntu Global Jam"
Slug: "UbuntuGlobalJam"
Author: "Kelemen Gábor"
---

A nemzetközi Ubuntu közösség idén tavasszal is megrendezi a [Global Jam][1]
nevű eseményt, amely remek alkalom a helyi közösségek számára, hogy
találkozzanak, tökéletesítsék az Ubuntut.

Ahogy korábban, most is részt veszünk, ezúttal élő rendezvénnyel, amely az őszi
akadémia helyszínén, az [Infopark Gábor Dénes út 2 alatti D épületében][2]
kerül megrendezésre. Az időpont március 27-28, 9-5 óráig. Aki nem tud
személyesen megjelenni, az online is részt vehet az *#ubuntu-hu* csatornán, aki
pedig igen, az a közösségi levelezőlistán jelezze.

Ezen a hétvégén a hamarosan kiadásra kerülő Lucid fordításán szeretnénk
dolgozni, konkrétan:

 - **K/Xubuntu slideshow:** ez egy érdekes feladat, a közös megoldása remek
   alkalom lenne a stílusunk finomítására
 - **Ubuntu doksi**, ha addig még nem készül el, egyelőre a Szoftverközpont
   kézikönyve várható, hogy addig még bekerül, mint nagyobb falat
 - **Csomagleírások:** legalább Szoftverközpont main tárolós csomagjai, meg a
   universeből a népszerűbb csomagok
 - **Grafikus felület fordításai**, ha addig nem készülnénk el.
 - **Fordítási hibák keresése** az új programok felületén – azoknak, akik
   egyszerűbb feladatokra vágynak

Nem kell megijedni, ezek annyira nem nagy falatok, és nem is az lenne a cél,
hogy minél többel kész legyünk, hanem a személyes találkozás, és a jó hangulat. 
Szombaton este előre láthatólag sörözéssel zárjuk a napot, egy később
megnevezendő helyszínen. Aki jön, az a következő kitűzők kiblogolásával jelezheti. Te ott leszel?

![Kitűző #1]({{< staticref "assets/images/blog/2010/ubuntu_globaljam_badge_1.png" >}} "Ubuntu Global Jam jelvény #1")
![Kitűző #2]({{< staticref "assets/images/blog/2010/ubuntu_globaljam_badge_2.png" >}} "Ubuntu Global Jam jelvény #2")

[1]: https://wiki.ubuntu.com/UbuntuGlobalJam
[2]: https://www.openstreetmap.org/way/77452145
