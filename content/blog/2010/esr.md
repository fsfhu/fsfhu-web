---
Title: "Foo mester Unix koanjai"
Date: 2010-02-24T19:00:00+02:00
Draft: false
Type: "post"
Summary: "Foo mester Unix koanjai"
Slug: "ESR"
Author: "Tímár András"
---
Tavaly decemberben kaptunk egy levelet:

> Tisztelt Alapítvány!
>
> Láttam, hogy belefogtak Eric Steven Raymond ragyogó írásainak fordításába.
> Hadd hívjam fel a figyelmet Master Foo rövid történeteire, amelyeket
> kollégáim körében gyakran idézünk.
> Mellékelem két fontos epizód fordítását is.
>
> Tisztelettel,
> Szabó Gergely

Köszönjük ezúton is, ma kitettem a [két epizódot][1]. Nagyon régen nem
frissítettük már az [esr.fsf.hu][2] weboldalt, kicsit háttérbe szorult az évek
során. Örömmel látjuk viszont, hogy az emberek tudnak róla, olvassák, és még
segítenek is a bővítésében. Valószínűleg ez lesz a működő modell, azaz a
közösség által beküldött fordítások ki fognak kerülni, de komolyabb bővítés az
Alapítvány részéről a közeli jövőben nem várható.

[1]: http://esr.fsf.hu/foo-mester.html
[2]: http://esr.fsf.hu

