# FSF.hu Web

![FSF.hu kezdőlap](doc/fsfhu.png "FSF.hu kezdőlap")

Ez a tároló tartalmazza a http://fsf.hu/ weblapjait. Legnagyobb részt statikus
HTML tartalomról van szó, minimális JavaScript komponensekkel.

## Összeállítás

A projekt a `hugo` statikus weboldal-generátort használja, azzal lehet
előállítani a végleges fájlokat. A célmappát a `-d` kapcsolóval lehet megadni:

    hugo -d result

## Futtatás helyi számítógépen

A projekt jellegéből adódan nem szükséges „rendes” webszerverrel használni, a
`hugo`-val is futtatható:

    git clone git@gitlab.com:fsfhu/fsfhu-web.git
    cd fsfhu-web
    hugo server

Ezután elérhető a http://localhost:1313/ címen.

# Blogbejegyzések készítése

A blogbejegyzések a `content/blog/<év>` mappába kerülnek, és a következőket
kell megadni a fejlécben:

    ---
    Title: "A bejegyzés címe"
    Date: 2019-08-04T15:43:37+02:00 (publikálás ISO8601 időbélyege)
    Draft: false (amíg csak vázlat, addig true)
    Type: "post" (enélkül nem jelenik meg az RSS-hírcsatornában)
    Summary: "Rövid leírás az RSS-hírcsatorna számára"
    Slug: "Rövid név, amely a horgony neve lesz
    Author: "Szerző neve"
    ---

